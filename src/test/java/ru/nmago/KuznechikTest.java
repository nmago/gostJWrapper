package ru.nmago;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.Security;

//this test works only on openjdk due to problems with cipher providers security
public class KuznechikTest {

    @BeforeClass
    public static void initGostKSProvider(){
        if (Security.getProvider("GostKSProvider") == null) {
            Security.addProvider(new GostKSProvider());
            System.out.println("Gost init finish");
        }
    }

    final String[] messages = new String[]{
            "data",
            "datblock",
            "oneblockandmore",
            "manymany many many many many many data, there is so many",
            "this message has to be 2 blocks exactly.",
            "блоки на русском",
            "немного спецсимволов !@#$%^&*()",
            "числа 012345679.......",
            "этот текст будет разбит на много блоков. этот текст будет разбит на много блоков. этот текст будет разбит на много блоков."
    };

    final byte[] manyData = new byte[]{
        0x01, 0x02, 0x03, 0x04, 0x32, 0x31, 0x30, 0x39, 0x38, 0x37, 0x36,
        0x37, 0x36, 0x35, 0x34, 0x33, 0x32, 0x31, 0x30, 0x39, 0x38, 0x37,
        0x36, 0x35, 0x34, 0x33, 0x32, 0x31, 0x30, 0x39, 0x38, 0x37, 0x36,
        0x35, 0x34, 0x33, 0x32, 0x31, 0x30, 0x39, 0x38, 0x37, 0x36, 0x35,
        0x34, 0x33, 0x32, 0x31, 0x30, 0x39, 0x38, 0x37, 0x36, 0x35, 0x34,
        0x33, 0x32, 0x31, 0x30, 0x32, 0x31, 0x30, 0x39, 0x38, 0x37, 0x36,
        0x37, 0x36, 0x35, 0x34, 0x33, 0x32, 0x31, 0x30, 0x39, 0x38, 0x37,
        0x36, 0x35, 0x34, 0x33, 0x32, 0x31, 0x30, 0x39, 0x38, 0x37, 0x36,
        0x35, 0x34, 0x33, 0x32, 0x31, 0x30, 0x39, 0x38, 0x37, 0x36, 0x35,
        0x34, 0x33, 0x32, 0x31, 0x30, 0x39, 0x38, 0x37, 0x36, 0x35, 0x34,
        0x33, 0x32, 0x31, 0x30, 0x35, 0x34, 0x33, 0x32, 0x31, 0x30, 0x39,
        0x38, 0x35, 0x34, 0x33, 0x32, 0x31, 0x30, 0x39, 0x38, 0x39, 0x38,
    };

    @Test
    public void finalFuncTest() throws Exception{

        SecretKeySpec sc = new SecretKeySpec("12345678912345671234567891234567".getBytes(StandardCharsets.UTF_8), "AES");

        for(String msg : messages){
            Cipher kuzEnc = Cipher.getInstance("Kuznechik", "GostKSProvider");
            Cipher kuzDecr = Cipher.getInstance("Kuznechik", "GostKSProvider");
            kuzEnc.init(Cipher.ENCRYPT_MODE, sc);
            kuzDecr.init(Cipher.DECRYPT_MODE, sc);
            byte[] enc = kuzEnc.doFinal(msg.getBytes());
            Assert.assertEquals(msg, new String(kuzDecr.doFinal(enc)));
        }
    }

    @Test
    public void manyDataWithPerfectUpdates() throws Exception{
        final int blockSize = 16;
        SecretKeySpec sc = new SecretKeySpec("12345678912345671234567891234567".getBytes(StandardCharsets.UTF_8), "AES");

        Cipher kuzEnc = Cipher.getInstance("Kuznechik", "GostKSProvider");
        Cipher kuzDecr = Cipher.getInstance("Kuznechik", "GostKSProvider");
        kuzEnc.init(Cipher.ENCRYPT_MODE, sc);
        kuzDecr.init(Cipher.DECRYPT_MODE, sc);

        byte[] encrypted = new byte[kuzEnc.getOutputSize(manyData.length)];

        for(int i = 0; i < manyData.length / blockSize; ++i){
            kuzEnc.update(manyData, i * blockSize, blockSize, encrypted, i * blockSize);
        }

        int lastBlockStart = manyData.length - (manyData.length % blockSize);
        kuzEnc.doFinal(manyData, lastBlockStart, manyData.length - lastBlockStart, encrypted, lastBlockStart);


        byte[] decrypted = new byte[encrypted.length];

        for(int i = 0; i < encrypted.length / blockSize - 1; ++i){
            kuzDecr.update(encrypted, i * blockSize, blockSize, decrypted, i * blockSize);
        }
        int how = kuzDecr.doFinal(encrypted, lastBlockStart, encrypted.length - lastBlockStart, decrypted, lastBlockStart);
        byte[] finalResult = new byte[decrypted.length - (blockSize) + how];
        System.arraycopy(decrypted, 0, finalResult, 0, decrypted.length - (blockSize) + how);
        Assert.assertArrayEquals(manyData, finalResult);

    }
}
