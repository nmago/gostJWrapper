package ru.nmago;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.security.MessageDigest;
import java.security.Security;

public class StribogTest {

    @BeforeClass
    public static void initGostKSProvider(){
        if (Security.getProvider("GostKSProvider") == null) {
            Security.addProvider(new GostKSProvider());
            System.out.println("Gost init finish");
        }
    }

    private final byte[] message1 = new byte[] { 0x32, 0x31, 0x30, 0x39,
            0x38, 0x37, 0x36, 0x35, 0x34, 0x33, 0x32, 0x31, 0x30, 0x39, 0x38,
            0x37, 0x36, 0x35, 0x34, 0x33, 0x32, 0x31, 0x30, 0x39, 0x38, 0x37,
            0x36, 0x35, 0x34, 0x33, 0x32, 0x31, 0x30, 0x39, 0x38, 0x37, 0x36,
            0x35, 0x34, 0x33, 0x32, 0x31, 0x30, 0x39, 0x38, 0x37, 0x36, 0x35,
            0x34, 0x33, 0x32, 0x31, 0x30, 0x39, 0x38, 0x37, 0x36, 0x35, 0x34,
            0x33, 0x32, 0x31, 0x30 };

    private final byte[] message2 = TestUtils.hexStringToByteArray("fbe2e5f0eee3c820fbeafaebef20fffbf0e1e0f0f520e0ed20e8ece0ebe5f0f2f120fff0eeec20f120faf2fee5e2202ce8f6f3ede220e8e6eee1e8f0f2d1202ce8f0f2e5e220e5d1");

    @Test
    public void emptyBytesTest() throws Exception{
        MessageDigest md = MessageDigest.getInstance("Stribog512");
        String actual = TestUtils.toHexStr(md.digest());
        String expected = "8a1a1c4cbf909f8ecb81cd1b5c713abad26a4cac2a5fda3ce86e352855712f36a7f0be98eb6cf51553b507b73a87e97946aebc29859255049f86aa09a25d948e";
        Assert.assertEquals("Empty hash", expected, actual);
    }

    @Test
    public void test256_1() throws Exception{
        final String expectedHash = "486f64c1917879417fef082b3381a4e211c324f074654c38823a7b76f830ad00";
        MessageDigest md = MessageDigest.getInstance("Stribog256");
        byte[] hash = md.digest(message1);
        String actualHash = TestUtils.toHexStr(hash);
        Assert.assertEquals("First 256-hash", expectedHash, actualHash);
    }

    @Test
    public void test512_1() throws Exception{
        final String expectedHash = "486f64c1917879417fef082b3381a4e211c324f074654c38823a7b76f830ad00fa1fbae42b1285c0352f227524bc9ab16254288dd6863dccd5b9f54a1ad0541b";
        MessageDigest md = MessageDigest.getInstance("Stribog512");
        byte[] hash = md.digest(message1);
        String actualHash = TestUtils.toHexStr(hash);
        Assert.assertEquals("First 512-hash", expectedHash, actualHash);
    }

    @Test
    public void test256_2() throws Exception{
        final String expectedHash = "28fbc9bada033b1460642bdcddb90c3fb3e56c497ccd0f62b8a2ad4935e85f03";
        MessageDigest md = MessageDigest.getInstance("Stribog256");
        byte[] hash = md.digest(message2);
        String actualHash = TestUtils.toHexStr(hash);
        Assert.assertEquals("Second 256-hash", expectedHash, actualHash);
    }

    @Test
    public void test512_2() throws Exception{
        final String expectedHash = "28fbc9bada033b1460642bdcddb90c3fb3e56c497ccd0f62b8a2ad4935e85f037613966de4ee00531ae60f3b5a47f8dae06915d5f2f194996fcabf2622e6881e";
        MessageDigest md = MessageDigest.getInstance("Stribog512");
        byte[] hash = md.digest(message2);
        String actualHash = TestUtils.toHexStr(hash);
        Assert.assertEquals("Second 512-hash", expectedHash, actualHash);
    }

}