package ru.nmago;

import java.util.Arrays;

public class TestUtils{

    public static void main(String[] a){
        String tes = "486f64c1917879417fef082b3381a4e211c324f074654c38823a7b76f830ad00fa1fbae42b1285c0352f227524bc9ab16254288dd6863dccd5b9f54a1ad0541b";
        byte[] ar = hexStringToByteArray(tes);
        System.out.println(Arrays.toString(ar));
        System.out.println(toHexStr(ar));
        System.out.println(toHexStr(ar).equals(tes));
    }

    public static String toHexStr(byte[] data){
        StringBuilder sb = new StringBuilder();
        for(byte b : data){
            int h = b & 0xFF;
            if(h < 0x10){
                sb.append("0");
            }
            sb.append(Integer.toHexString(h));
        }
        return sb.toString();
    }

    public static byte[] hexStringToByteArray(String s){
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }
}
