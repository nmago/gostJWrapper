package ru.nmago.kuznechik;

import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import ru.nmago.GostLib;
import ru.nmago.LibLoader;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import java.security.*;
import java.security.spec.AlgorithmParameterSpec;

/*
*
*  Russian Gost R 34.12-2015 crypto algorithm (aka Kuznechik) SPI
*  @Copyright gitlab.com/nmago [if this code does properly work]
*
 */

public class KuznechikSpi extends CipherSpi {

    private final int BLOCK_LEN = 16;
    private final int KEY_LEN = 32;
    private final int IV_LEN = 32;
    private final int CBC_CTX_LEN = 0x60;

    private final Pointer ptrCtx = new Memory(CBC_CTX_LEN);
    private final Pointer ptrKey = new Memory(KEY_LEN);
    private final Pointer ptrIV = new Memory(IV_LEN);

    private GostLib libInstanse;

    private AlgorithmParameters algorithmParameters;
    private Key key;
    private IvParameterSpec IV;
    private int mode;

    private byte[] dataBuffer = new byte[BLOCK_LEN];
    private int dataBufferLen = 0;

    private final byte[] defIV = new byte[]{
            0x12, 0x34, 0x56, 0x78, 0x10, (byte)0xab,  (byte)0xce,  (byte)0xf0,
            (byte)0xa1,  (byte)0xb2,  (byte)0xc3, 0x11, 0x11, 0x11, 0x01, 0x12,
            0x23, 0x34, 0x45, 0x56, 0x67, 0x78,  (byte)0x89, (byte) 0x90, 0x12,
            0x13, (byte) 0x14, 0x15, 0x16, 0x17, 0x18, 0x19
    };


    @Override
    protected void engineSetMode(String s) throws NoSuchAlgorithmException {
        if(!s.toUpperCase().equals("CBC")){
            throw new NoSuchAlgorithmException("Sorry, only CBC mode is supported so far");
        }
    }

    @Override //whatever
    protected void engineSetPadding(String s) throws NoSuchPaddingException {

    }

    @Override
    protected int engineGetBlockSize() {
        return BLOCK_LEN;
    }

    @Override
    protected int engineGetOutputSize(int inputLen) {
        if(mode == Cipher.ENCRYPT_MODE) {
            //we need exactly how input does + padding
            int padBytesLen = BLOCK_LEN - (inputLen % BLOCK_LEN);
            return inputLen + padBytesLen;
        }else{
            //in decrypt mode no more than inputLen
            return inputLen;
        }
    }

    @Override
    protected byte[] engineGetIV() {
        if(IV != null){
            return IV.getIV();
        }
        return defIV;
    }

    @Override
    protected AlgorithmParameters engineGetParameters() {
        return algorithmParameters;
    }

    @Override
    protected void engineInit(int mode, Key key, SecureRandom secureRandom) throws InvalidKeyException {
        if(key.getEncoded().length != KEY_LEN){
            throw new InvalidKeyException(
                    String.format("Key must be %d bytes, you provided %d", KEY_LEN, key.getEncoded().length)
            );
        }

        //TODO: make IV a little bit random (depends on key)

        this.mode = mode;
        this.IV = new IvParameterSpec(defIV);

        libInstanse = LibLoader.loadGostLib();
        ptrKey.write(0, key.getEncoded(), 0, key.getEncoded().length);
        ptrIV.write(0, IV.getIV(), 0, IV_LEN);
        libInstanse.init_cbc_14(ptrKey, ptrCtx, ptrIV, IV_LEN, null, null);
    }

    @Override
    protected void engineInit(int i, Key key, AlgorithmParameterSpec algorithmParameterSpec, SecureRandom secureRandom) throws InvalidKeyException, InvalidAlgorithmParameterException {
        //TODO: support of IV init
        this.engineInit(0, key, secureRandom);
    }

    @Override
    protected void engineInit(int i, Key key, AlgorithmParameters algorithmParameters, SecureRandom secureRandom) throws InvalidKeyException, InvalidAlgorithmParameterException {
        this.engineInit(0, key, secureRandom);
    }

    @Override
    protected byte[] engineUpdate(byte[] input, int inputOffset, int inputLen) {
        int totalLen = dataBufferLen + inputLen;
        if(totalLen >= BLOCK_LEN){
            int remainDataLen = totalLen % BLOCK_LEN;
            int toProcessLen = totalLen - remainDataLen; //complete blocks len to process
            Pointer dataToProcess = new Memory(toProcessLen);

            //taking complete blocks
            if(dataBufferLen > 0) {
                dataToProcess.write(0, dataBuffer, 0, dataBufferLen);
            }
            dataToProcess.write(dataBufferLen, input, inputOffset, inputLen - remainDataLen);

            //putting left data into buffer
            dataBufferLen = remainDataLen;
            if(dataBufferLen > 0) {
                System.arraycopy(input, inputOffset + inputLen - remainDataLen, dataBuffer, 0, dataBufferLen);
            }

            //processing complete blocks
            Pointer result = new Memory(toProcessLen);
            if(mode == Cipher.ENCRYPT_MODE){
                libInstanse.encrypt_cbc(ptrCtx, dataToProcess, result, toProcessLen);
            }else if(mode == Cipher.DECRYPT_MODE){
                libInstanse.decrypt_cbc(ptrCtx, dataToProcess, result, toProcessLen);
            }else{
                throw new RuntimeException("Given mode is not supported");
            }

            byte[] finalResult = new byte[toProcessLen];
            result.read(0, finalResult, 0, toProcessLen);

            return finalResult;
        }else{ //if we cant make even one block, put it for future process
            System.arraycopy(input, inputOffset, dataBuffer, dataBufferLen, inputLen);
            dataBufferLen = totalLen;
            return null;
        }
    }

    //TODO: make work for null input
    @Override
    protected int engineUpdate(byte[] input, int inputOffset, int inputLen, byte[] output, int outputOffset)
            throws ShortBufferException {
        if(output.length < outputOffset + inputLen - inputOffset){
            throw new ShortBufferException("Need a bigger output buffer");
        }

        byte[] result = engineUpdate(input, inputOffset, inputLen);
        if(result != null) {
            System.arraycopy(result, 0, output, outputOffset, result.length);
            return result.length;
        }else{
            return 0;
        }
    }

    @Override
    protected byte[] engineDoFinal(byte[] input, int inputOffset, int inputLen)
            throws IllegalBlockSizeException, BadPaddingException {

        int totalLen = dataBufferLen + inputLen;
        if(mode == Cipher.ENCRYPT_MODE){
            //hire memory for our data to process
            int paddedLen = engineGetOutputSize(totalLen);
            Pointer dataToEnc = new Memory(paddedLen);

            //putting data and padding
            if(dataBufferLen > 0) {
                dataToEnc.write(0, dataBuffer, 0, dataBufferLen);
            }
            if(inputLen > 0) {
                dataToEnc.write(dataBufferLen, input, inputOffset, inputLen);
            }
            libInstanse.padd(dataToEnc, totalLen, BLOCK_LEN);

            //encrypting
            Pointer result = new Memory(paddedLen);
            int er = libInstanse.encrypt_cbc(ptrCtx, dataToEnc, result, paddedLen);
            System.out.println("Enc: " + er);
            libInstanse.free_cbc(ptrCtx);

            byte[] finalResult = new byte[paddedLen];
            result.read(0, finalResult, 0, paddedLen);

            return finalResult;
        }else if(mode == Cipher.DECRYPT_MODE){
            if(totalLen % BLOCK_LEN == 0){
                //taking memory, filling it
                if(totalLen == 0){
                    throw new BadPaddingException("It seems you forgot to give me the last block");
                }
                Pointer dataToDecr = new Memory(totalLen);
                if(dataBufferLen > 0) {
                    dataToDecr.write(0, dataBuffer, 0, dataBufferLen);
                }
                dataToDecr.write(dataBufferLen, input, inputOffset, inputLen);

                //decrypting
                Pointer result = new Memory(totalLen);
                libInstanse.decrypt_cbc(ptrCtx, dataToDecr, result, totalLen);
                libInstanse.free_cbc(ptrCtx);

                //calc len without padding and leaving it ((padding) bytes) there
                int unpadLen = (int) libInstanse.unpadd(result, totalLen);
                byte[] finalResult = new byte[unpadLen];
                result.read(0, finalResult, 0, unpadLen);

                return finalResult;
            }else{
                throw new IllegalBlockSizeException("Final data length is not multiply of blocksize ("
                        + BLOCK_LEN + " bytes), you provided " + totalLen + " bytes");
            }
        }else{
            throw new RuntimeException("Given mode is not supported");
        }

    }

    @Override
    protected int engineDoFinal(byte[] input, int inputOffset, int inputLen, byte[] output, int outputOffset)
            throws ShortBufferException, IllegalBlockSizeException, BadPaddingException {
        if(output.length < outputOffset + inputLen - inputOffset){
            throw new ShortBufferException("Need a bigger output buffer");
        }

        byte[] result = engineDoFinal(input, inputOffset, inputLen);
        System.arraycopy(result, 0, output, outputOffset, result.length);
        return result.length;
    }
}
