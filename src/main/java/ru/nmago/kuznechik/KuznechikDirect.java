package ru.nmago.kuznechik;


import javax.crypto.*;
import java.security.*;
import java.security.spec.AlgorithmParameterSpec;

/*
*   Class for direct access to KuznechikSPI methods (which are protected)
*   {for future testing}
*/

public class KuznechikDirect extends KuznechikSpi {
    public void test_engineSetMode(String s) throws NoSuchAlgorithmException {
        this.engineSetMode(s);
    }

    public void test_engineSetPadding(String s) throws NoSuchPaddingException {
        this.engineSetPadding(s);
    }

    public int test_engineGetBlockSize() {
        return this.engineGetBlockSize();
    }

    public int test_engineGetOutputSize(int inputLen) {
        return this.engineGetOutputSize(inputLen);
    }

    public byte[] test_engineGetIV() {
        return this.engineGetIV();
    }

    public AlgorithmParameters test_engineGetParameters() {
        return this.engineGetParameters();
    }

    public void test_engineInit(int mode, Key key, SecureRandom secureRandom) throws InvalidKeyException {
        this.engineInit(mode, key, secureRandom);
    }

    public void test_engineInit(int i, Key key, AlgorithmParameterSpec algorithmParameterSpec, SecureRandom secureRandom) throws InvalidKeyException, InvalidAlgorithmParameterException {
        this.engineInit(i, key, algorithmParameterSpec, secureRandom);
    }

    public void test_engineInit(int i, Key key, AlgorithmParameters algorithmParameters, SecureRandom secureRandom) throws InvalidKeyException, InvalidAlgorithmParameterException {
        this.engineInit(i, key, algorithmParameters, secureRandom);
    }

    public byte[] test_engineUpdate(byte[] bytes, int i, int i1) {
        return engineUpdate(bytes, i, i1);
    }

    public int test_engineUpdate(byte[] bytes, int i, int i1, byte[] bytes1, int i2) throws ShortBufferException {
        return engineUpdate(bytes, i, i1, bytes1, i2);
    }

    public byte[] test_engineDoFinal(byte[] bytes, int i, int i1) throws IllegalBlockSizeException, BadPaddingException {
        return engineDoFinal(bytes, i, i1);
    }

    public int test_engineDoFinal(byte[] bytes, int i, int i1, byte[] bytes1, int i2) throws ShortBufferException, IllegalBlockSizeException, BadPaddingException {
        return engineDoFinal(bytes, i, i1, bytes1, i2);
    }

}
