package ru.nmago;

import ru.nmago.kuznechik.KuznechikSpi;
import ru.nmago.stribog.Stribog256;
import ru.nmago.stribog.Stribog512;

import java.security.*;

public class GostKSProvider extends Provider{

    /**
     * Constructs a provider with the specified name, version number,
     * and information.
     *
     */
    GostKSProvider() {
        super("GostKSProvider", 1.5, "Provider of Stribog & Kuznechik");
        AccessController.doPrivileged((PrivilegedAction) () -> {
            put("MessageDigest.Stribog256", Stribog256.class.getCanonicalName());
            put("MessageDigest.Stribog512", Stribog512.class.getCanonicalName());

            put("Cipher.Kuznechik", KuznechikSpi.class.getCanonicalName());
            put("Cipher.Kuznechik SupportedPaddings", "NOPADDING");
            put("Cipher.Kuznechik SupportedModes", "CBC");
            put("Cipher.Kuznechik SupportedKeyFormats", "RAW");
            return null;
        });

    }

}
