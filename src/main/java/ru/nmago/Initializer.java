package ru.nmago;


import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.Security;
import java.util.Arrays;

public class Initializer {
    void test(){
        System.out.println("Hi");
        LibLoader.loadGostLib();
    }


    public static void main(String[] argc) throws Exception{
        if (Security.getProvider("GostKSProvider") == null) {
            Security.addProvider(new GostKSProvider());
            System.out.println("Gost init finish");
        }
        String messagestr = "a";
        byte[] message = hexStringToByteArray("fbe2e5f0eee3c820fbeafaebef20fffbf0e1e0f0f520e0ed20e8ece0ebe5f0f2f120fff0eeec20f120faf2fee5e2202ce8f6f3ede220e8e6eee1e8f0f2d1202ce8f0f2e5e220e5d1");
        MessageDigest md = MessageDigest.getInstance("Stribog512");
        System.out.println(message.length);
        byte[] hash = md.digest();
        printHex(message);
        printHex(hash);
    }

    private static void printHex(byte[] digest) {
        for (byte b : digest) {
            int iv = b & 0xFF;
            if (iv < 0x10) {
                System.out.print('0');
            }
            System.out.print(Integer.toHexString(iv));
        }
        System.out.println();
    }

    public static byte[] hexStringToByteArray(String s){
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }
}
