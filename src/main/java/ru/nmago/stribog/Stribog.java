package ru.nmago.stribog;

import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import ru.nmago.GostLib;
import ru.nmago.LibLoader;

import java.security.MessageDigestSpi;

public abstract class Stribog extends MessageDigestSpi{
    private StribogContex ctx;
    private static GostLib libInstanse;

    static{
        libInstanse = LibLoader.loadGostLib();
    }

    byte[] getDigest(){
        initEngineIfNot();
        int hashLen = 512 / 8; //64 bytes
        Pointer ptrHash = new Memory(hashLen);
        libInstanse.STRIBOG_finalize(ctx, ptrHash);
        byte[] hash = new byte[hashLen];
        ptrHash.read(0, hash, 0, hashLen);
        return hash;
    }

    @Override
    protected void engineUpdate(byte input) {
        initEngineIfNot();
        //System.out.println("Called upd with : " + input );
        Pointer b = new Memory(1);
        b.write(0, new byte[]{input}, 0, 1);
        libInstanse.STRIBOG_add(ctx, b, 1);
    }

    @Override
    protected void engineUpdate(byte[] input, int offset, int len) {
        initEngineIfNot();
        //System.out.println("Called upd with : " + input.length + " bits of data" );
        Pointer bytes = new Memory(len - offset);
        bytes.write(0, input, offset, len);
        libInstanse.STRIBOG_add(ctx, bytes, len - offset);
    }

    @Override
    protected void engineReset() {
        ctx = new StribogContex();
        libInstanse.STRIBOG_init(ctx);
        System.out.println("Reset called");
    }

    protected void initEngineIfNot(){
        if(ctx == null){
            ctx = new StribogContex();
            libInstanse.STRIBOG_init(ctx);
        }
    }
}
