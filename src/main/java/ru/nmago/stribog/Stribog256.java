package ru.nmago.stribog;


public class Stribog256 extends Stribog{
    @Override
    protected byte[] engineDigest() {
        byte[] hash32 = new byte[32];
        System.arraycopy(getDigest(), 0, hash32, 0, hash32.length);
        return hash32;
    }
}
