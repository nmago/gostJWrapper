package ru.nmago.stribog;

import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

public class StribogContex extends Structure{
    /*
    byte N[64];
	byte hash[64];
	byte left;
	byte remainder[64];
	byte v512[64];
	byte v0[64];
	byte EPSILON[64];
     */
    public byte[] N = new byte[64];
    public byte[] hash = new byte[64];
    public byte left;
    public byte[] remainder = new byte[64];
    public byte[] v512 = new byte[64];
    public byte[] v0 = new byte[64];
    public byte[] EPSILON = new byte[64];

    @Override
    protected List<String> getFieldOrder() {
        List<String> ll = Arrays.asList(
                "N",
                "hash",
                "left",
                "remainder",
                "v512",
                "v0",
                "EPSILON"
        );
        //System.out.println(Arrays.toString(ll.toArray()));
        return ll;
    }
}
