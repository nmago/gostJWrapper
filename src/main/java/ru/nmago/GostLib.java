package ru.nmago;

import com.sun.jna.Function;
import com.sun.jna.Library;
import com.sun.jna.Pointer;
import ru.nmago.stribog.StribogContex;

public interface GostLib extends Library{

    /**
     * Инициализация контекста шифрования в режиме CBC для алгоритма "кузнечик"
     *
     * [in] key ключ
     * [out] ctx контекст cbc
     * [in] iv синхропосылка
     * [in] ivLength длина синхропосылки
     * [in] print функция логирования
     * [in] print_uint функция логирования
     * @return 0 если все преобразование прошло успешно
     */
    int init_cbc_14(Pointer key, Pointer ctx, Pointer iv, long ivLength, Function print, Function print_uint);

    /**
     * Выполнение зашифрования информации в режиме простой замены с зацеплением (CBC)
     * для данных кратных размеру блока
     *
     * [in] ctx контекст CBC
     * [in] indata открытый текст
     * [out] outdata зашифрованный текст
     * [in] length длина текста
     * @return 0 если все преобразование прошло успешно
     */
    int encrypt_cbc(Pointer ctx, Pointer indata, Pointer outdata, long length);


    /**
     * Выполнение рсшифрования информации в режиме простой замены с зацеплением (CBC)
     * для данных кратных размеру блока
     *
     * [in] ctx контекст CBC
     * [in] indata зашифрованный текст
     * [out] outdata расшифрованный текст
     * [in] length длина текста
     * @return 0 если все преобразование прошло успешно
     */
    int decrypt_cbc(Pointer ctx, Pointer indata, Pointer outdata, long length);


    /**
     * Дополнение данных до размера блока.
     *
     * [in] data сообщение. Память под данные data должна быть выделена достаточная для дополнения.
     * [in] length размер сообщения
     * [in] blockLen длина блока
     * @return размер сообщения
     */
    long padd(Pointer data, long length, long blockLen);


    /**
     * Удаление дополненных данных. При ошибках возвращается значение -1
     *
     * [in] data сообщение
     * [in] length размер сообщения
     * @return размер сообщения
     */
    long unpadd(Pointer data, long length);


    /**
     * Удаление контекста cbc
     *
     * [in] ctx контекст cbc
     */
    void free_cbc(Pointer ctx);


    //--------------------------STRIBOG--------------------------------

    /**
     * Получение размера контекста для стрибога
     *
     * @return размер контекста для Стрибога в байтах
     */
    long STRIBOG_getCtxSize();


    /** Инициализация Стрибог
     *
     * [in] ctx контекст
     */
    void STRIBOG_init(StribogContex ctx);

    /** Добавление данных к вычислению хеша
     *
     * [in] ctx контекст
     * [in] msg данные
     * [in] len длина данных в байтах
     */
    void STRIBOG_add(StribogContex ctx, Pointer msg, long len);

    /** Получения результат - хеша
     *
     * [in] ctx контекст
     * [in] out хеш 64 байта
     */
    void  STRIBOG_finalize(StribogContex ctx, Pointer out);
}
