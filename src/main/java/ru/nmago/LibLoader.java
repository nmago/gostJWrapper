package ru.nmago;

import com.sun.jna.Native;

import java.io.File;

public class LibLoader {
    private static GostLib INSTANCE;
    private static String libBaseDir = "lib-native";

    public static GostLib loadGostLib(){
        if(INSTANCE == null) {
            String libPath = getOSDependentPath();
            System.out.println("Loading: " + libPath);
            INSTANCE = Native.loadLibrary(libPath, GostLib.class);
        }
        return INSTANCE;
    }

    private static String getOSDependentPath(){
        String OS = System.getProperty("os.name").toLowerCase();
        String xtype = System.getProperty("sun.arch.data.model");
        StringBuilder result = new StringBuilder(libBaseDir + File.separator);
        if(OS.contains("win")){
            result.append("win32").append(File.separator);
            if(xtype.equals("64")){ //DLL, x64
                result.append("libgost-x64.dll");
            }else{ //DLL, x86
                result.append("libgost-x86.dll");
            }
            return result.toString();
        }else if(OS.contains("nix") || OS.contains("nux") || OS.contains("aix")){ //unix
            result.append("linux").append(File.separator);
            if(xtype.equals("64")){
                result.append("libgost.so");
            }else{
                throw new RuntimeException("We do not support linux x86");
            }
            return result.toString();
        }else{
            throw new RuntimeException("Unsupported OS, cant load gost library");
        }
    }
}
